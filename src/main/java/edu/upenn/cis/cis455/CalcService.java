package edu.upenn.cis.cis455;

import static spark.Spark.*;

public class CalcService {
    public static void main(String[] args) {
        // Function calls here are set to spark.Spark as per the "import static" above.
        // Before the Spark framework launches, we want to configure the server to
        // start on a different port.
    	port(45555);
        
        // When does it launch? See https://sparkjava.com/documentation#getting-started
        // which says it starts (a) if we set a port, or (b) if we declare a "route" handler

        // And here's a route handler, which calls a Java lambda function with a request-response
        // object pair and returns the String result as content in a response.
        // See https://sparkjava.com/documentation#routes and https://javadoc.io/doc/com.sparkjava/spark-core/latest/index.html
        // 
        // The method signature should be get(String url, Route route) where 
        // Route's default function is handle(Request req, Response res)
        get("/hello", (req, res) -> "Hello World");

        System.out.println("Waiting to handle requests!");
        
        // Generally this isn't necessary, but it blocks until the web server is initialized
        // The server should stay running in the background until Ctrl-C is hit
        awaitInitialization();
    }
}
